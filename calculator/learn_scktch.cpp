#include <iostream>
#include "cal_lib.h"
using namespace std;

int main() 
{
	double a = 0;
	double b = 0;
	char opr = 0;

	cout << "a: ";
	cin >> a;
	cout << "b: ";
	cin >> b;
	cout << "Operator: ";
	cin >> opr;

	switch (opr) {
		case '+':
			cout << add (a, b) << endl;
			break;
		case '-':
			cout << reduce (a, b) << endl;
			break;
		case '/':
			cout << *(dividing (&a, &b)) << endl;
			break;
		case '*':
			cout << *(multiplication (&a, &b)) << endl;
			break;
		default:
			break;
		
	}	
	return 0;
}
